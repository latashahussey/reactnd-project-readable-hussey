exports.port = process.env.PORT || 3001
exports.origin = process.env.ORIGIN || `http://readable-app-hussey.surge.sh:${exports.port}`
