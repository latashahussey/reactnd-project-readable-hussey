const clone = require('clone')

let db = {}

const defaultData = {
    "8xf0y6ziyjabvozdd253nd": {
        id: '8xf0y6ziyjabvozdd253nd',
        timestamp: 1467166872634,
        title: 'Udacity is the best place to learn React',
        body: 'Everyone says so after all. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eget mauris pharetra et ultrices neque ornare aenean. Massa ultricies mi quis hendrerit dolor magna eget est. Arcu dui vivamus arcu felis bibendum ut tristique. Sed turpis tincidunt id aliquet. Arcu dui vivamus arcu felis bibendum ut tristique et egestas. Interdum varius sit amet mattis vulputate enim. Semper viverra nam libero justo laoreet sit. Eget mauris pharetra et ultrices neque ornare aenean. Amet dictum sit amet justo donec enim diam vulputate. Elementum nisi quis eleifend quam adipiscing vitae proin. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Nunc id cursus metus aliquam. Tempor id eu nisl nunc.Suspendisse faucibus interdum posuere lorem ipsum dolor sit. Aliquet bibendum enim facilisis gravida. In nibh mauris cursus mattis. In aliquam sem fringilla ut morbi tincidunt. Sit amet est placerat in egestas erat imperdiet sed. Nibh praesent tristique magna sit amet purus. Risus nullam eget felis eget nunc lobortis mattis aliquam. Enim tortor at auctor urna nunc id cursus metus. Et magnis dis parturient montes nascetur ridiculus. Ipsum nunc aliquet bibendum enim facilisis gravida neque. Dui ut ornare lectus sit amet est. Quis enim lobortis scelerisque fermentum dui faucibus. Rhoncus est pellentesque elit ullamcorper. Urna porttitor rhoncus dolor purus non enim. Enim sit amet venenatis urna cursus eget nunc scelerisque.',
        author: 'thingtwo',
        category: 'react',
        voteScore: 6,
        deleted: false,
        commentCount: 2
    },
    "6ni6ok3ym7mf1p33lnez": {
        id: '6ni6ok3ym7mf1p33lnez',
        timestamp: 1468489767190,
        title: 'Learn Redux in 10 minutes!',
        body: 'Just kidding. It takes more than 10 minutes to learn technology.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Habitant morbi tristique senectus et netus et. Leo a diam sollicitudin tempor id. Lacus vel facilisis volutpat est velit egestas. Felis eget nunc lobortis mattis aliquam faucibus purus.Commodo viverra maecenas accumsan lacus vel. Tortor at auctor urna nunc. Porttitor massa id neque aliquam vestibulum morbi blandit cursus. Sit amet nulla facilisi morbi tempus iaculis. Ipsum faucibus vitae aliquet nec. Facilisi nullam vehicula ipsum a arcu cursus. Cras adipiscing enim eu turpis egestas. Pellentesque elit ullamcorper dignissim cras tincidunt. Nec sagittis aliquam malesuada bibendum. Pharetra et ultrices neque ornare aenean euismod elementum. Aenean vel elit scelerisque mauris. Non pulvinar neque laoreet suspendisse interdum consectetur.',
        author: 'thingone',
        category: 'redux',
        voteScore: -5,
        deleted: false,
        commentCount: 0
    },
    "55f0y6ziyjabvozdd253nd": {
        id: '55f0y6ziyjabvozdd253nd',
        timestamp: 1468466872634,
        title: 'Javascript Made Simple',
        body: 'Everyone says so after all. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eget mauris pharetra et ultrices neque ornare aenean. Massa ultricies mi quis hendrerit dolor magna eget est. Arcu dui vivamus arcu felis bibendum ut tristique. Sed turpis tincidunt id aliquet. Arcu dui vivamus arcu felis bibendum ut tristique et egestas. Interdum varius sit amet mattis vulputate enim. Semper viverra nam libero justo laoreet sit. Eget mauris pharetra et ultrices neque ornare aenean. Amet dictum sit amet justo donec enim diam vulputate. Elementum nisi quis eleifend quam adipiscing vitae proin. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Nunc id cursus metus aliquam. Tempor id eu nisl nunc.Suspendisse faucibus interdum posuere lorem ipsum dolor sit. Aliquet bibendum enim facilisis gravida. In nibh mauris cursus mattis. In aliquam sem fringilla ut morbi tincidunt. Sit amet est placerat in egestas erat imperdiet sed. Nibh praesent tristique magna sit amet purus. Risus nullam eget felis eget nunc lobortis mattis aliquam. Enim tortor at auctor urna nunc id cursus metus. Et magnis dis parturient montes nascetur ridiculus. Ipsum nunc aliquet bibendum enim facilisis gravida neque. Dui ut ornare lectus sit amet est. Quis enim lobortis scelerisque fermentum dui faucibus. Rhoncus est pellentesque elit ullamcorper. Urna porttitor rhoncus dolor purus non enim. Enim sit amet venenatis urna cursus eget nunc scelerisque.',
        author: 'thingseven',
        category: 'javascript',
        voteScore: 22,
        deleted: false,
        commentCount: 0
    },
    "99i6ok33m7mf1p33lnez": {
        id: '99i6ok33m7mf1p33lnez',
        timestamp: 1468478767190,
        title: 'ES6 Rocks!',
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Habitant morbi tristique senectus et netus et. Leo a diam sollicitudin tempor id. Lacus vel facilisis volutpat est velit egestas. Felis eget nunc lobortis mattis aliquam faucibus purus.Commodo viverra maecenas accumsan lacus vel. Tortor at auctor urna nunc. Porttitor massa id neque aliquam vestibulum morbi blandit cursus. Sit amet nulla facilisi morbi tempus iaculis. Ipsum faucibus vitae aliquet nec. Facilisi nullam vehicula ipsum a arcu cursus. Cras adipiscing enim eu turpis egestas. Pellentesque elit ullamcorper dignissim cras tincidunt. Nec sagittis aliquam malesuada bibendum. Pharetra et ultrices neque ornare aenean euismod elementum. Aenean vel elit scelerisque mauris. Non pulvinar neque laoreet suspendisse interdum consectetur.',
        author: 'noob12',
        category: 'javascript',
        voteScore: 10,
        deleted: false,
        commentCount: 0
    }

}

function getData(token) {
    let data = db[token]
    if (data == null) {
        data = db[token] = clone(defaultData)
    }
    return data
}

function getByCategory(token, category) {
    return new Promise((res) => {
        let posts = getData(token)
        let keys = Object.keys(posts)
        let filtered_keys = keys.filter(key => posts[key].category === category && !posts[key].deleted)
        res(filtered_keys.map(key => posts[key]))
    })
}

function get(token, id) {
    return new Promise((res) => {
        const posts = getData(token)
        res(
            posts[id].deleted
            ? {}
            : posts[id])
    })
}

function getAll(token) {
    return new Promise((res) => {
        const posts = getData(token)
        let keys = Object.keys(posts)
        let filtered_keys = keys.filter(key => !posts[key].deleted)
        res(filtered_keys.map(key => posts[key]))
    })
}

function add(token, post) {
    return new Promise((res) => {
        let posts = getData(token)

        posts[post.id] = {
            id: post.id,
            timestamp: post.timestamp,
            title: post.title,
            body: post.body,
            author: post.author,
            category: post.category,
            voteScore: 1,
            deleted: false,
            commentCount: 0
        }

        res(posts[post.id])
    })
}

function vote(token, id, option) {
    return new Promise((res) => {
        let posts = getData(token)
        post = posts[id]
        switch (option) {
            case "upVote":
                post.voteScore = post.voteScore + 1
                break
            case "downVote":
                post.voteScore = post.voteScore - 1
                break
            default:
                console.log(`posts.vote received incorrect parameter: ${option}`)
        }
        res(post)
    })
}

function disable(token, id) {
    return new Promise((res) => {
        let posts = getData(token)
        posts[id].deleted = true
        res(posts[id])
    })
}

function edit(token, id, post) {
    return new Promise((res) => {
        let posts = getData(token)
        for (prop in post) {
            posts[id][prop] = post[prop]
        }
        res(posts[id])
    })
}

function incrementCommentCounter(token, id, count) {
    const data = getData(token)
    if (data[id]) {
        data[id].commentCount += count
    }
}

module.exports = {
    get,
    getAll,
    getByCategory,
    add,
    vote,
    disable,
    edit,
    getAll,
    incrementCommentCounter
}
