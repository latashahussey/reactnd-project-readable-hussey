/* Readable API Utility Helper */

// store api server url
const api = "http://localhost:3001"

// Generate a unique token for storing post data on api Server
let token = '6ireqvRtyTzwItmFTiaE'


// save Authorization token
const headers = {
    'Accept': 'application/json',
    'Authorization': token,
    'Content-Type' : 'application/json'
}

// fetch METHODS

// fetch all categories in JSON format
export const fetchCategories = () =>
    fetch(`${api}/categories`, { headers })
        .then( response => response.json())

// fetch all posts in JSON format
export const fetchPosts = () =>
    fetch(`${api}/posts`, { headers })
        .then( response => response.json())

// fetch posts by ID
export const fetchPostById = (postId) =>
    fetch(`${api}/posts/${postId}`, { headers })
        .then( response => response.json())

// fetch posts by category
export const fetchPostsByCategory = (category) =>
    fetch(`${api}/${category}/posts`, {headers})
        .then( response => response.json())

// fetch comments for single post by postId
export const fetchPostCommentsById = (postId) =>
    fetch(`${api}/posts/${postId}/comments`, { headers })
        .then( response => response.json())

// fetch single comment by id
export const fetchCommentById = (commentId) =>
    fetch(`${api}/comments/${commentId}`, { headers })
        .then( response => response.json())





// POST METHODS

// Add a new post
export const addPost = (post) =>
    fetch(`${api}/posts`, {
        method: 'POST',
        headers,
        body: JSON.stringify(post)
    }).then( response => response.json())

// Add new comment
export const addComment = (comment) =>
    fetch(`${api}/comments`, {
        method: 'POST',
        headers,
        body: JSON.stringify(comment)
    }).then( response => response.json())

// Add upVote for a post
export const addPostUpVote = (postId) =>
    fetch(`${api}/posts/${postId}`, {
        method: 'POST',
        headers ,
        body: JSON.stringify({postId, "option":"upVote"})
    }).then( response => response.json())

// Add downVote for a post
export const addPostDownVote = (postId) =>
    fetch(`${api}/posts/${postId}`, {
        method: 'POST',
        headers ,
        body: JSON.stringify({postId, "option":"downVote"})
    }).then( response => response.json())

// Add upVote for a comment
export const addCommentUpVote = (commentId) =>
    fetch(`${api}/comments/${commentId}`, {
        method: 'POST',
        headers ,
        body: JSON.stringify({commentId, "option":"upVote"})
    }).then( response => response.json())

// Add downVote for a comment
export const addCommentDownVote = (commentId) =>
    fetch(`${api}/comments/${commentId}`, {
        method: 'POST',
        headers ,
        body: JSON.stringify({commentId, "option":"downVote"})
    }).then( response => response.json())



// PUT METHODS

// Edit an existing post
export const editPost = (post) =>
    fetch(`${api}/posts/${post.id}`, {
        method: 'PUT',
        headers,
        body: JSON.stringify(post)
    }).then( response => response.json())

// Edit an existing comment
export const editComment = (comment) =>
    fetch(`${api}/comments/${comment.id}`, {
        method: 'PUT',
        headers,
        body: JSON.stringify(comment)
    }).then( response => response.json())


// DELETE METHODS

// Delete a post
export const deletePost = (postId) =>
    fetch(`${api}/posts/${postId}`, {
        method: 'DELETE',
        headers
    }).then( response => response.json())

// Delete a comment
export const deleteComment = (commentId) =>
    fetch(`${api}/comments/${commentId}`, {
        method: 'DELETE',
        headers
    }).then( response => response.json())
