import {combineReducers} from 'redux'
import postReducer from './posts'
import commentReducer from './comments'
import categoryReducer from './categories'

// Create root reducer by combining all reducers to single prop of app state
const rootReducer = combineReducers({
  posts: postReducer, // manages all posts
  comments: commentReducer, // manages all comments
  categories: categoryReducer // manages all categories
})

// Make reducers available to app
export default rootReducer
