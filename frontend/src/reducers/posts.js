import sortBy from 'sort-by'
import {
  POST_GET,
  POST_SORT,
  POST_ADD,
  POST_EDIT,
  POST_DELETE,
  POST_UP_VOTE,
  POST_DOWN_VOTE,
  POST_FILTER_BY_CATEGORY
} from '../actions/types'

// CREATE POSTS REDUCER - accepts current state and action to be performed then decides what to do with the state based on the action type

let newState

export default function postReducer(state = [], action) {
  switch (action.type) {
    case POST_GET:
      return action.posts
    case POST_SORT:
      return [...state].sort(sortBy(action.option))
    case POST_ADD:

      return [
        ...state, {
          ...action.post
        }
      ]
    case POST_EDIT:
      return action.post
    case POST_DELETE:
      if (state.length >= 0) {
        return state.filter(posts => posts.id !== action.post)
      }
    return [...state]
    case POST_UP_VOTE:
      newState = [...state]
      if (state.length >= 0) {
        const indexOfStatePost = state.findIndex((post) => {
          return post.id === action.post.id})
        newState.splice(indexOfStatePost, 1, action.post)
      }
      return newState
    case POST_DOWN_VOTE:
      newState = [...state]
      if (state.length >= 0) {
        const indexOfStatePost = state.findIndex((post) => {
          return post.id === action.post.id})
        newState.splice(indexOfStatePost, 1, action.post)
      }
      return newState
    case POST_FILTER_BY_CATEGORY:
      if (action.posts.category !== null) {
        return action.posts
      }
      return [...state]
    default:
      return state;
  }
}
