import {CATEGORY_GET} from '../actions/types'

// CREATE CATEGORIES REDUCER - accepts current state and action to be performed then decides what to do with the state based on the action type
export default function categoryReducer(state = [], action) {
  switch (action.type) {
    case CATEGORY_GET:
      return action.categories
    default:
      return state
  }
}
