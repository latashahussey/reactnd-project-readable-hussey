import {
  COMMENT_GET,
  COMMENT_ADD,
  COMMENT_EDIT,
  COMMENT_DELETE,
  COMMENT_UP_VOTE,
  COMMENT_DOWN_VOTE
} from '../actions/types'

// CREATE COMMENTS REDUCER - accepts current state and action to be performed then decides what to do with the state based on the action type
let newState

export default function commentReducer(state = [], action) {
  switch (action.type) {
    case COMMENT_GET:
      if (!action.comments)
        return null
      else
        return action.comments
      case COMMENT_ADD:
      return [
        ...state, {
          ...action.comment
        }
      ]
    case COMMENT_EDIT:
      return action.comment
    case COMMENT_DELETE:
      return state.filter(comments => comments.id !== action.comment)
    case COMMENT_UP_VOTE:
      newState = [...state]
      if (state.length >= 0) {
        const indexOfStateComment = state.findIndex((comment) => {
          return comment.id === action.comment.id})
        newState.splice(indexOfStateComment, 1, action.comment)
      }
      return newState
    case COMMENT_DOWN_VOTE:
      newState = [...state]
      if (state.length >= 0) {
        const indexOfStateComment = state.findIndex((comment) => {
          return comment.id === action.comment.id})
        newState.splice(indexOfStateComment, 1, action.comment)
      }
      return newState
    default:
      return state;
  }
}
