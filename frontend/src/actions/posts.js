import * as ReadableAPI from '../utils/ReadableAPI'
import {
  POST_GET,
  POST_SORT,
  POST_ADD,
  POST_EDIT,
  POST_DELETE,
  POST_UP_VOTE,
  POST_DOWN_VOTE,
  POST_FILTER_BY_CATEGORY
} from './types'

export function receivePosts(posts) {
  return {
    type: POST_GET,
    posts: posts
  }
}

export function sortPosts(option) {
  return {
    type: POST_SORT,
    option
  }
}

export function deletePost(post) {
  return {
    type: POST_DELETE,
    post: post.id
  }
}

export function editPost(post) {
  return {
    type: POST_EDIT,
    post
  }
}

export function addPost(post) {
  return {
    type: POST_ADD,
    post
  }
}

export function addPostDownVote(post) {
  return {
    type: POST_DOWN_VOTE,
    post
  }
}

export function addPostUpVote(post) {
  return {
    type: POST_UP_VOTE,
    post
  }
}

export function filterPostsByCategory(posts) {
  return {
    type: POST_FILTER_BY_CATEGORY,
    posts: posts
  }
}

// POST ACTION CREATORS - dispatches action
export function getPosts(sortOption) {
  return dispatch => {
    return ReadableAPI.fetchPosts()
    .then(posts => {
      if (sortOption) {
        dispatch(sortPosts(posts, sortOption))
      } else {
        dispatch(receivePosts(posts))
      }
    })
  }
}

export function getPostById(postId) {
  return dispatch => {
    return ReadableAPI.fetchPostById(postId)
    .then(post => dispatch(receivePosts(post)))
  }
}

export function removePost(postId) {
  return dispatch => {
    return ReadableAPI.deletePost(postId)
    .then(post => dispatch(deletePost(post)))
  }
}

export function modifyPost(post) {
  return dispatch => {
    return ReadableAPI.editPost(post)
    .then(post => dispatch(editPost(post)))
  }
}

export function newPost(post) {
  return dispatch => {
    return ReadableAPI.addPost(post)
    .then(post => dispatch(addPost(post)))
  }
}

export function scoreDownPost(post) {
  return dispatch => {
    return ReadableAPI.addPostDownVote(post)
    .then(post => dispatch(addPostDownVote(post)))
  }
}

export function scoreUpPost(post) {
  return dispatch => {
    return ReadableAPI.addPostUpVote(post)
    .then(post => dispatch(addPostUpVote(post)))
  }
}

export function getPostsByCategory(category) {
  if (category === 'show-all') {
    return dispatch => {
      dispatch(getPosts())
    }
  } else {
    return dispatch => {
      return ReadableAPI.fetchPostsByCategory(category)
      .then(posts => dispatch(filterPostsByCategory(posts)))
    }
  }
}

export function sortAllPosts(option) {
  return dispatch => {
    dispatch(sortPosts(option))
  }
}
