export const CATEGORY_GET = 'CATEGORY_GET'

export const POST_GET = 'POST_GET'
export const POST_SORT = 'POST_SORT'
export const POST_ADD = 'POST_ADD'
export const POST_EDIT = 'POST_EDIT'
export const POST_DELETE = 'POST_DELETE'
export const POST_UP_VOTE = 'POST_UP_VOTE'
export const POST_DOWN_VOTE = 'POST_DOWN_VOTE'
export const POST_FILTER_BY_CATEGORY = 'POST_FILTER_BY_CATEGORY'

export const COMMENT_GET = 'COMMENT_GET'
export const COMMENT_ADD = 'COMMENT_ADD'
export const COMMENT_EDIT = 'COMMENT_EDIT'
export const COMMENT_DELETE = 'COMMENT_DELETE'
export const COMMENT_UP_VOTE = 'COMMENT_UP_VOTE'
export const COMMENT_DOWN_VOTE = 'COMMENT_DOWN_VOTE'
