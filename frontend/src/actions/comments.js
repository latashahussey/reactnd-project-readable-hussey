import * as ReadableAPI from '../utils/ReadableAPI'
import {
  COMMENT_GET,
  COMMENT_ADD,
  COMMENT_EDIT,
  COMMENT_DELETE,
  COMMENT_UP_VOTE,
  COMMENT_DOWN_VOTE
} from './types'

export function receiveComments(comments) {
  return {
    type: COMMENT_GET,
    comments: comments
  }
}

export function deleteComment(comment) {
  return {
    type: COMMENT_DELETE,
    comment: comment.id
  }
}

export function editComment(comment) {
  return {
    type: COMMENT_EDIT,
    comment
  }
}

export function addComment(comment) {
  return {
    type: COMMENT_ADD,
    comment
  }
}

export function addCommentDownVote(comment) {
  return {
    type: COMMENT_DOWN_VOTE,
    comment
  }
}

export function addCommentUpVote(comment) {
  return {
    type: COMMENT_UP_VOTE,
    comment
  }
}

// CREATE COMMENT ACTION CREATORS - dispatches action

export function getComments(postId) {
  return dispatch => {
    return ReadableAPI.fetchPostCommentsById(postId)
    .then(comments => dispatch(receiveComments(comments)))
  }
}

export function getCommentById(commentId) {
  return dispatch => {
    return ReadableAPI.fetchCommentById(commentId)
    .then(comment => dispatch(receiveComments(comment)))
  }
}

export function removeComment(commentId) {
  return dispatch => {
    return ReadableAPI.deleteComment(commentId)
    .then(comment => dispatch(deleteComment(comment)))
  }
}

export function modifyComment(comment) {
  return dispatch => {
    return ReadableAPI.editComment(comment)
    .then(postId => dispatch(editComment(comment)))
  }
}

export function newComment(comment) {
  return dispatch => {
    return ReadableAPI.addComment(comment)
    .then(comment => dispatch(addComment(comment)))
  }
}

export function scoreDownComment(comment) {
  return dispatch => {
    return ReadableAPI.addCommentDownVote(comment)
    .then(comment => dispatch(addCommentDownVote(comment)))
  }
}

export function scoreUpComment(comment) {
  return dispatch => {
    return ReadableAPI.addCommentUpVote(comment)
    .then(comment => dispatch(addCommentUpVote(comment)))
  }
}
