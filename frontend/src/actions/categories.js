import * as ReadableAPI from '../utils/ReadableAPI'
import {CATEGORY_GET} from './types'

export function getCategories() {
  return dispatch => {
    return ReadableAPI.fetchCategories()
    .then(categories => dispatch(receiveCategories(categories)))
  }
}

export function receiveCategories(categories) {
  return {
    type: CATEGORY_GET,
    categories
  }
}
