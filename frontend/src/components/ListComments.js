import React from 'react'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'
import {withRouter} from 'react-router'
import Comment from './Comment'
import {Row, Col, Button} from 'reactstrap'
import '../styles/css/fontawesome-all.css';

const ListComments = ({match}) => (
  <Row className="bg-light pr-3 pl-3">
    <Col>
      {/* Comments */}
      <Row>
        <Col md="6">
          {/* Comment Heading */}
          <h3 className="text-left pt-4 pb-2">
            <i className="fal fa-comments-alt mr-2"></i>
            Comments
          </h3>
        </Col>
        <Col className="m-auto text-right" md="6">
          <Link to={`/${match.params.category}/${match.params.id}/comments/add`}>
            <Button className="mr-0 btn-add" size="md">
              <i className="far fa-comment-alt-plus mr-2"></i>
              Add a Comment
            </Button>
          </Link>
        </Col>
      </Row>
      <Row>
        <Col>
          {/* Display user comments for this post */}
          <Comment match={match}/>
        </Col>
      </Row>
    </Col>
    {/* end Comments */}
  </Row>
)

ListComments.propTypes = {
  match: PropTypes.object.isRequired
}
// make component available to app
export default withRouter(ListComments)
