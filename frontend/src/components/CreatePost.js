import React, {Component} from 'react'
import {Link, Redirect} from 'react-router-dom'
import {withRouter} from 'react-router';
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {getPostById, getPosts, newPost, modifyPost} from '../actions/posts'
import {getCategories} from '../actions/categories'
import serializeForm from 'form-serialize'
import randomize from 'randomatic'
import {
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Button
} from 'reactstrap'

class CreatePost extends Component {
    // Required properties for component
    static propTypes = {
      match: PropTypes.object.isRequired
    }

    constructor() {
      super();

      this.state = {
        triggerRedirect: false
      }
    }

    componentWillMount() {

        if (this.props.match.params.id) {
          let postId = this.props.match.params.id
          this.props.getPostById(postId)
        }
        this.props.getCategories()
    }

    /**
     * handleSubmit Handles form submission, then passes data
     * to appropriate action
     * @param  {object} form serialized data from form
     */
    handleSubmit = (form) => {
      form.preventDefault()
      this.setState({triggerRedirect: true})
      const formValues = serializeForm(form.target, {
        hash: true
      })
      let post = {}

      if (window.location.pathname.includes('add')) {
        post = {
          id: randomize('Aa0', 24),
          timestamp: Date.now(),
          ...formValues
        }
        this.props.newPost(post)
      } else {
        post = {
          id: this.props.match.params.id,
          timestamp: Date.now(),
          ...formValues
        }
        this.props.modifyPost(post)
      }
    }

    render() {

      // Update form title based on action type
      const formType = (() => {
        if (window.location.pathname.includes('add')) {
            return 'New'
        }
        return 'Edit'
      })();

      const {posts, categories, location} = this.props
      const {from} = location.state || '/'
      const {triggerRedirect} = this.state

      return (
        <div>
            {/* Breadcrumbs */}
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb bg-white pl-0">
              <li className="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li className="breadcrumb-item">
                <Link to="/posts">Posts</Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">{formType} Post</li>
            </ol>
          </nav>

          {/* CreatePost */}
          <Row key={posts.id}>
            <Col>
              <Form onSubmit={this.handleSubmit} action="/" method="POST">
                <FormGroup row>
                  {/* Form Heading */}
                  <Col className="text-left">
                    <h2>{formType} Post</h2>
                  </Col>
                </FormGroup>
                <FormGroup row className="mt-4">
                  {/* Text Field: postTitle */}
                  <Col className="text-left">
                    <Label for="postTitle">Post Title</Label>
                    <Input type="text" name="title" id="postTitle" bsSize="lg" defaultValue={posts.title} required/>
                  </Col>
                </FormGroup>
                <FormGroup row className="mt-4">
                  {/* Text Field: postAuthor */}
                  <Col className="text-left">
                    <Label for="postAuthor">Post Author</Label>
                    <Input type="text" name="author" id="postAuthor" bsSize="lg" defaultValue={posts.author} required />
                  </Col>
                  {/* Select Field: postCategory */}
                  <Col className="text-left">
                    <Label for="postCategory">Category</Label>
                    <Input type="select" name="category" id="postCategory" bsSize="lg" defaultValue={posts.category} required >
                      <option default="default">Select Category</option>
                      {categories.map((category) => (
                        <option key={category.name} value={category.name}>{category.name}</option>))}
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row className="mt-4">
                  {/* Textarea Field: postBody */}
                  <Col className="text-left">
                    <Label for="postBody">Post Body</Label>
                    <Input style={{
                            height: 400
                        }} type="textarea" name="body" id="postBody" bsSize="lg" defaultValue={posts.body} required />
                  </Col>
                </FormGroup>
                <FormGroup row className="mt-4 text-right">
                  {/* Button:  Cancel/Submit */}
                  <Col>
                    <Link to="/">
                        <Button className="btn-cancel mr-3" size="lg">Cancel</Button>
                    </Link>

                    <Button type="submit" className="btn-submit mr-0" size="lg" value="Save">Save</Button>
                  </Col>
                </FormGroup>
              </Form>
              {triggerRedirect && (<Redirect to={from || '/'}/>)}
            </Col>
          </Row>
        </div>
    ) // end return
  } //end render
} // end CreatePost components

/**
 * mapStateToProps - connects component to application state
 * @param  {object} state state of application
 * @return {object}
 */
function mapStateToProps(state) {
    return {
      posts: state.posts,
      categories: state.categories
    }
}

/**
 * mapDispatchToProps - makes imported functions accessible
 * to component via this.props
 * @return {function}
 */
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ //dispatches action that prompt state changes
        getPostById: getPostById,
        getPosts: getPosts,
        getCategories: getCategories,
        newPost: newPost,
        modifyPost: modifyPost
    }, dispatch)
}

// allows component direct access to read/write app state
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(CreatePost)
)
