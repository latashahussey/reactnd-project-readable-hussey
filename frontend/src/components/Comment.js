import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {
  getComments,
  removeComment,
  scoreUpComment,
  scoreDownComment
  } from '../actions/comments'
import {Link} from 'react-router-dom'
import {withRouter} from 'react-router';
import PropTypes from 'prop-types'
import dateFormat from 'dateformat'
import {Row, Col, Card, CardBody, Button} from 'reactstrap'
import '../styles/css/fontawesome-all.css';

class Comment extends Component {

  constructor(props) {
    super(props);
    this.removeComment = this.props.removeComment.bind(this)
    this.scoreUpComment = this.props.scoreUpComment.bind(this)
    this.scoreDownComment = this.props.scoreDownComment.bind(this)
  }

  static propTypes = {
    match: PropTypes.object.isRequired
  }

  componentWillMount() {
    const parentId = this.props.match.params.id
    this.props.getComments(parentId)
  }

  render() {
    const {comments} = this.props

    return (
      <div>
      {/* User Comment */}

      {
        comments.length === 0
          ? <p className="p-4 text-center lead">
              No comments here. You should add one.</p>
          : comments.length > 0 && comments.map((comment) => (
            <div key={comment.id}>
            <Card className="mt-4 mb-5">
              <CardBody>
                <Row>
                  <Col className="text-left" md="8">
                    {/* Comment Author */}
                    <h6>
                      <i className="fal fa-user-circle mr-2"></i>
                      {comment.author}
                    </h6>
                  </Col>
                  {/* Edit/Delete Comment */}
                  <Col md="4" className="text-right">
                    <Link
                      className="btn-edit"
                      to={`/${this.props.match.params.category}/${comment.parentId}/comments/${comment.id}/edit`}>
                      <i className="fal fa-pencil-alt mr-2"></i>
                    </Link>
                    <Button
                      color="link"
                      onClick={() => this.removeComment(comment.id)}>
                      <i className="fal fa-trash-alt ml-2 mb-2"></i>
                    </Button>
                  </Col>
                </Row>
                <Row>
                  {/* Comment Body */}
                  <Col className="text-left pt-2">
                    <p>{comment.body}</p>
                    {/* Comment Posted Date */}
                    <span className="text-muted">
                      <small>Posted on {dateFormat(comment.timestamp, "dddd, mmmm dS, yyyy, h:MM:ss TT")}
                      </small>
                    </span>
                  </Col>
                </Row>
                <Row>
                  {/* Comment Score */}
                  <Col md="6">
                    <span className="float-left mt-2">Score: {comment.voteScore}</span>
                  </Col>
                  {/* Comment Voting Buttons */}
                  <Col md="6 text-right">
                    <Button
                      color="link"
                      type="submit"
                      name="option"
                      value="upVote"
                      onClick={() => this.scoreUpComment(comment.id)}>
                      <i className="fal fa-thumbs-up mr-2 btn-vote"></i>
                    </Button>
                    <Button
                      color="link"
                      type="submit"
                      name="option"
                      value="downVote"
                      onClick={() => this.scoreDownComment(comment.id)}>
                      <i className="fal fa-thumbs-down mr-2 btn-vote"></i>
                    </Button>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </div>
        ))
      }
      {/* end User Comment */}
    </div>) // end return
  } // end render
} // end Comment component

/**
 * mapStateToProps - connects component to application state
 * @param  {object} comments post comments
 */
function mapStateToProps({comments}) {
  return {comments}
}

/**
 * mapDispatchToProps - makes imported functions accessible to component via this.props
 * @return {function}
 */
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ //dispatches action that prompt state changes
    getComments: getComments,
    removeComment: removeComment,
    scoreUpComment: scoreUpComment,
    scoreDownComment: scoreDownComment
  }, dispatch)
}

// allows component direct access to read/write app state
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Comment))
