import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {getCategories} from '../actions/categories'
import {Link} from 'react-router-dom'
import {Row, Col, Card, CardHeader, CardBody} from 'reactstrap'

class ListCategories extends Component {
  componentDidMount() {
    this.props.getCategories()
  }

  render() {

    const {categories} = this.props

    return (<Row className="mb-4">
      {/* Categories */}
      <Col>
        <Card>
          {/* Category Widget Title */}
          <CardHeader tag="h4">Categories</CardHeader>
          {/* List of Categories */}
          <CardBody>
            <ul className="text-left">
              {
                categories.map((category) => (
                  <div key={category.path}>
                    <Link to={`/${category.path}/posts`}>
                      <li>{category.name}</li>
                    </Link>
                  </div>
                ))
              }
              <Link to="/show-all/posts/">
                <li>Show All Posts</li>
              </Link>
            </ul>
          </CardBody>
        </Card>
      </Col>
      {/* end Categories */}
    </Row>) // end return
  } // end render
} // end ListCategories components

/**
 * mapStateToProps - connects component to application state
 * @param  {object} state state of application
 * @return {object}       state.categories as this.props.categories
 */
function mapStateToProps({ categories }) {
  return {
    categories
  }
}

/**
 * mapDispatchToProps - makes imported functions accessible to component via this.props
 * @return {function}
 */
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ //dispatches action that prompt state changes
    getCategories: getCategories
  }, dispatch)
}

// allows component direct access to read/write app state
export default connect(mapStateToProps, mapDispatchToProps)(ListCategories)
