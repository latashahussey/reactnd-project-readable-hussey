import React from 'react'
import PropTypes from 'prop-types'
import {withRouter} from 'react-router'
import {Row, Col} from 'reactstrap'
import Post from './Post'

const ListPosts = ({match}) => (
  <div>
  {/* Posts */}
    <Row>
      <Col>
        {/* Display Post Component */}
        <Post match={match}/>
      </Col>
    </Row>
  </div>
) 

ListPosts.propTypes = {
  match: PropTypes.object.isRequired
}

// Make component available to app
export default withRouter(ListPosts)
