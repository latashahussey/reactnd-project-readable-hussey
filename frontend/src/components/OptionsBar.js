import React from 'react'
import {Link} from 'react-router-dom'
import '../styles/css/fontawesome-all.css';
import {
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Button
} from 'reactstrap'

const OptionsBar = ({handleSortChange}) => (
  <Row>
    {/* New Post and Sort Options */}
    <Col md="6">
      <Link to="/posts/add">
        <Button color="success" size="md" className="btn-add w-50 float-left">
          <i className="fas fa-plus text-left mr-2"></i>Add New Post
        </Button>
      </Link>
    </Col>
    <Col md="6">
      <Form >
        <FormGroup row>
          <Col>
            <Row>
              <Col md="4" className="text-right pr-0 pt-1">
                <Label size="md" for="sortby" className="clearfix mb-0 align-middle">Sort By</Label>
              </Col>
              <Col>
                <Input onChange={handleSortChange} type="select" name="sort" id="sortby" bsSize="md">
                  <option default="default">Select Option</option>
                  <option value="author">Author</option>
                  <option value="timestamp">Date (ASC)</option>
                  <option value="-timestamp">Date (DESC)</option>
                  <option value="category">Category (ASC)</option>
                  <option value="-category">Category (DESC)</option>
                  <option value="-commentCount">Most Comments</option>
                  <option value="commentCount">Fewest Comments</option>
                  <option value="title">Title (ASC)</option>
                  <option value="-title">Title (DESC)</option>
                  <option value="-voteScore">Most Votes</option>
                  <option value="voteScore">Least Votes</option>
                </Input>
              </Col>
            </Row>
          </Col>
        </FormGroup>
      </Form>
    </Col>
    {/* end New Post and Sort Options */}
  </Row>
)

// Makes component available to app
export default OptionsBar
