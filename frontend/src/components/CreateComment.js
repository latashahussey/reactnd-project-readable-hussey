import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {getCommentById, newComment, modifyComment} from '../actions/comments'
import serializeForm from 'form-serialize'
import randomize from 'randomatic'
import {Link, Redirect} from 'react-router-dom'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import {
    Row,
    Col,
    Form,
    FormGroup,
    Label,
    Input,
    Button
} from 'reactstrap'


class CreateComment extends Component {
    // Required properties for component
	static propTypes = {
    match: PropTypes.object.isRequired
	}

  constructor(){
    super();
    this.state = {
      triggerRedirect: false
    }
  }

  componentWillMount() {
    if(this.props.match.params.commentId) {
        let commentId = this.props.match.params.commentId
        this.props.getCommentById(commentId)
    }
  }

  /**
   * handleSubmit Handles form submission, then passes data to appropriate action
   * @param  {object} form serialized data from form
   */
  handleSubmit = (form) => {
    form.preventDefault()
    this.setState({triggerRedirect: true})
    const formValues = serializeForm(form.target, {hash: true})
    let comment = {}

    if (window.location.pathname.includes('add')) {
      comment = {
        id: randomize('Aa0', 24),
        timestamp: Date.now(),
        parentId: this.props.match.params.parentId,
        ...formValues
      }
      this.props.newComment(comment)
    } else {
        comment = {
          id: this.props.match.params.commentId,
          timestamp: Date.now(),
          parentId: this.props.match.params.parentId,
          ...formValues
        }
      this.props.modifyComment(comment)
    }
  }

  render() {

    // Update form title based on action type
    const formType = (() => {
      if (window.location.pathname.includes('add')) {
          return 'New'
      }
      return 'Edit'
    })();

    const {comment, location} = this.props
    const { from } = location.state || '/'
    const { triggerRedirect } = this.state

    return (
      <div > {/* Breadcrumbs */}
        <nav aria-label="breadcrumb">
            <ol className="breadcrumb bg-white pl-0">
              <li className="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li className="breadcrumb-item active">Comment</li>
              <li className="breadcrumb-item active" aria-current="page">{formType} Comment</li>
            </ol>
          </nav>

          {/* Create a Comment Form Modal */}
        <Row>
          <Col>
            <Row>
              <Col>
                <h2 className="text-left pb-2">{formType}
                  Comment</h2>
              </Col>
            </Row>
            <Row key={comment.id}>
              <Col>
                <Form onSubmit={this.handleSubmit} action="/" method="POST">
                  <FormGroup row>
                    <Col>
                      {/* Text Field: userName */}
                      <Label for="commentAuthor">Your Name</Label>
                      <Input className="mb-3" bsSize="lg" type="text" name="author" id="commentAuthor" defaultValue={comment.author}/>
                      {/* Textarea Field: userComment */}
                      <Label for="commentBody">Comment</Label>
                      <Input className="mb-3" bsSize="lg" style={{
                          height: 200
                        }} type="textarea" name="body" id="commentBody" defaultValue={comment.body}/>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col className="text-right">
                      {/* Button: Cancel */}
                      <Link to="/">
                        <Button className="btn-cancel mr-3" size="lg">Cancel</Button>
                      </Link>
                      {/* Button: Submit */}
                      <Button className="btn-add mr-0" size="lg">Save</Button>
                    </Col>
                  </FormGroup>
                </Form>

                {triggerRedirect && (
                  <Redirect
                    to={
                      from || `/post/${this.props.match.params.parentId}/details`
                    }
                    />)}

              </Col>
            </Row>
          </Col>
        </Row>
        {/* end Create a Comment Form */}
      </div>
    )
  }
}

/**
 * mapStateToProps - connects component to application state
 * @param  {object} state state of application
 * @return {object}
 */
function mapStateToProps(state) {
    return {
        comment: state.comments
    }
}

/**
 * mapDispatchToProps -  makes imported functions accessible to component via this.props
 * @return {function}
 */
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ //dispatches action that prompt state changes
        getCommentById: getCommentById,
        newComment: newComment,
        modifyComment: modifyComment
    }, dispatch)
}

// allows component direct access to read/write app state
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateComment))
