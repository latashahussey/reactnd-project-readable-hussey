import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {getPosts, getPostsByCategory, removePost, scoreUpPost, scoreDownPost} from '../actions/posts'
import {Link} from 'react-router-dom'
import dateFormat from 'dateformat'
import {
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button
} from 'reactstrap'
import '../styles/css/fontawesome-all.css';

class Post extends Component {
  constructor(props) {
    super(props)
    this.removePost = this.props.removePost.bind(this)
    this.scoreUpPost = this.props.scoreUpPost.bind(this)
    this.scoreDownPost = this.props.scoreDownPost.bind(this)
  }

  componentWillMount() {
    this.props.getPosts()
  }

  componentDidUpdate(nextProps) {
    // Filter posts if user has selected a category
    if (this.props.match.url !== nextProps.match.url) {
      this.props.getPostsByCategory(this.props.match.params.category)
    }
  }

  render() {
    const {posts} = this.props

    return (
      <div>
        {/* Post */}

        {/* If user selects category without posts, display message, otherwise, show posts for selected category or all of them */}
        {
          posts.length === 0
            ? <p className="p-4 text-center lead">
              Sorry, we could not find any posts in this category.</p>
            : posts.length > 0 && posts.map((post) => (
              <div key={post.id}>
              <Card className="mt-2 mb-3">
                <CardBody className="text-left">
                  <Row>
                    <Col md="9">
                      {/* Post Title */}
                      <Link to={`/${post.category}/${post.id}/details`}>
                        <CardTitle>{post.title}</CardTitle>
                      </Link>
                      {/* Post Author and Category */}
                      <CardSubtitle className="text-muted font-lg">
                        <i className="fal fa-user-circle mr-2 "></i>{post.author}
                        <i className="fal fa-folder-open ml-3 mr-2 "></i>{post.category}
                      </CardSubtitle>
                    </Col>
                    {/* Edit/Delete Post Options */}
                    <Col md="3" className="text-right">
                      <Link className="btn-edit" to={`/${post.category}/${post.id}/edit`}>
                        <i className="fal fa-pencil-alt mr-2"></i>
                      </Link>
                      <Button
                        color="link"
                        className="btn-delete"
                        onClick={() => this.removePost(post.id)}>
                        <i className="fal fa-trash-alt ml-2 mb-2"></i>
                      </Button>
                    </Col>
                  </Row>
                  {/* Post Body */}
                  <Row className="pt-3">
                    <Col>
                      <p>{post.body.substring(0, 150)}...</p>
                    </Col>
                  </Row>
                  {/* Posted Date */}
                  <Row>
                    <Col>
                      <span className="text-muted">
                        <small>Posted on {dateFormat(post.timestamp, "dddd, mmmm dS, yyyy, h:MM:ss TT")}</small>
                      </span>
                    </Col>
                  </Row>
                  <hr/>
                  <Row>
                    {/* Post Comment */}
                    <Col md="4">
                      <p>
                        <span className=" mr-1">Comments:
                        </span>{post.commentCount}</p>
                    </Col>
                    {/* Post Score */}
                    <Col md="4">
                      <p>
                        <span className=" mr-1">Score:
                        </span>{post.voteScore}</p>
                    </Col>
                    {/* Post Voting Buttons */}
                    <Col md="4" className="text-right">
                      <span className="text-right">
                        <Button
                          color="link"
                          type="submit"
                          name="option"
                          value="upVote"
                          onClick={() => this.scoreUpPost(post.id)}>
                          <i className="fal fa-thumbs-up mr-2 btn-vote"></i>
                        </Button>

                      </span>
                      <span className="text-right">
                        <Button
                          color="link"
                          type="submit"
                          name="option"
                          value="downVote"
                          onClick={() => this.scoreDownPost(post.id)}>
                          <i className="fal fa-thumbs-down mr-2 btn-vote"></i>
                        </Button>
                      </span>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </div>
          ))
        }
      {/* end Post */}
    </div>) // end render
  } // end return
} // end Post component

/**
 * mapStateToProps - connects component to application state
 * @param  {object} posts available posts
 * @return {object}
 */
function mapStateToProps({posts}) {
  return {posts}
}

/**
 * mapDispatchToProps - makes imported functions accessible to component via this.props
 * @return {function}
 */
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ //dispatches action that prompt state changes
    getPosts: getPosts,
    getPostsByCategory: getPostsByCategory,
    removePost: removePost,
    scoreUpPost: scoreUpPost,
    scoreDownPost: scoreDownPost
  }, dispatch)
}

// allows component direct access to read/write app state
export default connect(mapStateToProps, mapDispatchToProps)(Post)
