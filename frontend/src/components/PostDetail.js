import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {
  getPostById,
  removePost,
  scoreUpPost,
  scoreDownPost
  } from '../actions/posts'
import {Link, Redirect} from 'react-router-dom'
import {withRouter} from 'react-router';
import ListComments from './ListComments'
import PropTypes from 'prop-types'
import dateFormat from 'dateformat'
import {Row, Col, Button} from 'reactstrap'

class PostDetail extends Component {
  constructor(props){
    super(props)

    this.state = {
      isDeletedPost: false
    }

    this.scoreUpPost = this.props.scoreUpPost.bind(this)
    this.scoreDownPost = this.props.scoreDownPost.bind(this)
  }

  static propTypes = {
    match: PropTypes.object.isRequired
  }

  componentWillMount() {
    const postId = this.props.match.params.id
    this.props.getPostById(postId)
  }

  handleDeletedPost = (postId) => {
    this.props.removePost(postId)
    .then(() => {
      this.setState({
        isDeletedPost: true
      })
    })
  }

  render() {

    const {post} = this.props

    // Prevent users from accessing deleted posts
    if(this.state.isDeletedPost === true) {
      return <Redirect to="/" />
    } else if (Object.keys(post).length === 0) {
      return (<p>Sorry, this post was deleted.</p>)
    } else {
      return (
        <div>
        {/* Breadcrumbs */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-white pl-0">
            <li className="breadcrumb-item">
              <Link to="/">Home</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/">Posts</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">{post.category}</li>
            <li className="breadcrumb-item active" aria-current="page">{post.title}</li>
          </ol>
        </nav>

        {/* Post Detail */}
        <Row key={post.id}>
          <Col >
            <Row>
              <Col>
                <Row className="pt-1">
                  {/* Post Title */}
                  <Col md="9">
                    <h2 className="text-left pb-2">{post.title}</h2>
                  </Col>
                  {/* Edit/Delete Post */}
                  <Col md="3" className="text-right">
                    <Link className="btn-edit" to={`/post/${post.id}/edit`}>
                      <i className="fal fa-pencil-alt mr-2 fa-lg"></i>
                    </Link>
                    <Button color="link"
                        onClick={() => this.handleDeletedPost(post.id) }>
                        <i className="fal fa-trash-alt ml-2 mb-2 fa-lg"></i>
                    </Button>
                  </Col>
                </Row>
                <Row className="text-muted">
                  {/* Post Author, Category */}
                  <Col className="text-left">
                    <i className="fal fa-user-circle mr-2 "></i>
                    {post.author}
                    <i className="fal fa-folder-open ml-3 mr-2 "></i>
                    {post.category}
                  </Col>
                </Row>
                <Row className="pt-3">
                  {/* Number of Comments and Score */}
                  <Col md="6">
                    <span className="float-left mr-4">
                      Comments: {post.commentCount}</span>
                    <span className="float-left">Score: {post.voteScore}</span>
                  </Col>
                  {/* Voting Buttons */}
                  <Col md="6" className="text-right">
                    <Button
                      color="link"
                      type="submit"
                      name="option"
                      value="upVote"
                      onClick={() => this.scoreUpPost(post.id)}>
                      <i className="fal fa-thumbs-up mr-2 btn-vote"></i>
                    </Button>
                    <Button
                      color="link"
                      type="submit"
                      name="option"
                      value="downVote"
                      onClick={() => this.scoreDownPost(post.id)}>
                      <i className="fal fa-thumbs-down mr-2 btn-vote"></i>
                    </Button>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  {/* Post Date */}
                  <Col className="text-left">
                    <small>
                      Posted on {dateFormat(post.timestamp, "dddd, mmmm dS, yyyy, h:MM:ss TT")}
                    </small>
                  </Col>
                </Row>
                <hr/>
                <Row>
                  {/* Post Body */}
                  <Col className="text-left">
                    <p>{post.body}</p>
                  </Col>
                </Row>
              </Col>
            </Row>
            {/* Display Comments Component */}
            <ListComments match={this.props.match}/>
          </Col>
        </Row>
        </div>
      )

    }


  }

}

/**
 * mapStateToProps - connects component to application state
 * @param  {object} state application state
 */
function mapStateToProps(state) {
  return {
    post: state.posts
  }
}

/**
 * mapDispatchToProps - makes imported functions accessible to component via this.props
 * @return {function}
 */
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ //dispatches action that prompt state changes
    getPostById: getPostById,
    removePost: removePost,
    scoreDownPost: scoreDownPost,
    scoreUpPost: scoreUpPost
  }, dispatch)
}

// allows component direct access to read/write app state
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PostDetail))
