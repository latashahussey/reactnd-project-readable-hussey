import React from 'react'
import '../styles/css/fontawesome-all.css';

const Error404 = () => (
  <div className="text-center">
    <i className="fal fa-exclamation-triangle fa-6x"></i>
    <h2 className="display-2">404</h2>
    <p>Whoa! You won't find that here.</p>
  </div>
)

// Makes component available to app
export default Error404
