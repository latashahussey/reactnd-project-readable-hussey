import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {Route, Switch} from 'react-router-dom'
import OptionsBar from './OptionsBar'
import ListPosts from './ListPosts'
import ListCategories from './ListCategories'
import CreatePost from './CreatePost'
import PostDetail from './PostDetail'
import CreateComment from './CreateComment'
import Error404 from './Error404'
import {sortAllPosts} from '../actions/posts'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Jumbotron,
  Container,
  Row,
  Col
} from 'reactstrap'
import '../styles/css/fontawesome-all.css';

class App extends Component {

  constructor() {
    super();
    this.toggle = this.toggle.bind(this);
    this.handleSortChange = this.handleSortChange.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  /**
     * @description handleSortChange Handles change to user-selected sort option
     * @param  {string} option name of property used
     * to sort posts (e.g. author, most comments, etc)
     * @return {object}        returns current state of
     * posts sorted by selected property
     */
  handleSortChange = (option) => {
    this.props.sortAllPosts(option.target.value)
  }

  render() {
    // optionsBar holds 'Add New Post' and 'Sort by' options, hide it when adding, editing, or viewing posts
    const optionsBar = (() => {
      if (!window.location.pathname.includes('edit') && !window.location.pathname.includes('details') && !window.location.pathname.includes('add')) {
        return <OptionsBar handleSortChange={this.handleSortChange}/>
      }
      return null;
    })();

    return (<div className="App">
      {/* Display Navbar */}
      <Navbar color="dark" dark expand="md">
        <NavbarBrand href="/">Readable</NavbarBrand>
        <NavbarToggler onClick={this.toggle}/>
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink href="/">Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                href="http://latashahussey.com/#about"
                target="_blank"
                rel="noopener noreferrer">
                  About
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                href="http://latashahussey.com/#contact"
                target="_blank"
                rel="noopener noreferrer">
                  Contact
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
      {/* end Navbar */}

      {/* Welcome - Jumbotron */}
      <Jumbotron fluid className="text-center">
        <Container>
          {/* <img src={hero}/> */}
          <p className="welcome text-white display-3 m-5 dark-bg">
            Welcome to Readable!
          </p>
        </Container>

      </Jumbotron>
      {/* end Jumbotron */}

      {/* Main */}
      <Container>
        <Row>
          <Col md="9">
            {/* Show/Hide Options Bar Placeholder */}
            {optionsBar}

            {/* MAIN CONTENT */}
            <Switch>
              <Route exact path="(/|/posts)" render={() => (
                <ListPosts/>)}/>
              <Route exact path='/:category/posts' render={() => (
                <ListPosts/>)}/>
              <Route exact path="/:category/:id/details" render={() => (
                <PostDetail match={this.props.match}/>)}/>
              <Route exact path="/posts/add" render={() => (
                <CreatePost/>)}/>
              <Route exact path='/:category/:id/edit' render={() => (
                <CreatePost match={this.props.match}/>)}/>
              <Route exact path='/:category/:parentId/comments/add'
                render={() => (
                <CreateComment match={this.props.match}/>)}/>
              <Route exact path='/:category/:parentId/comments/:commentId/edit/'
                render={() => (
                <CreateComment match={this.props.match}/>)}/>
              {/* Display 404 error on invalid URLs */}
              <Route component={Error404}/>
            </Switch>
          </Col>

          {/* Sidebar */}
          <Col md="3" sm="12">
            {/* Display SideBar Widget */}
            <ListCategories/>
          </Col>
          {/* end Sidebar */}
        </Row>
        {/* end Container.row */}
      </Container>
      {/* end Main Container */}

      {/* Footer */}
      <footer className="text-white p-5 mt-5 text-center">
        <Container fluid>
          {/* Social Media */}
          <Row>
            <Col>
              <ul className="list-inline">
                <li className="list-inline-item">
                  <a href="https://www.twitter/latashahussey" target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-twitter fa-2x"></i>
                  </a>
                </li>
                <li className="list-inline-item">
                  <a href="https://gitlab.com/latashahussey" target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-gitlab fa-2x"></i>
                  </a>
                </li>
                <li className="list-inline-item">
                  <a href="https:/linkedin.com/in/latashahussey" target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-linkedin fa-2x"></i>
                  </a>
                </li>
                <li className="list-inline-item">
                  <a href="http://latashahussey.com" target="_blank" rel="noopener noreferrer">
                    <i className="fal fa-globe mr-2 fa-2x text-white"></i>
                  </a>
                </li>
              </ul>
            </Col>
          </Row>
          {/* end Social Media */}

          {/* Development Credit */}
          <Row>
            <Col>
              <p>Designed with
                <i className="fab fa-react"></i>
                <a href="https://reactstrap.github.io/" target="_blank" rel="noopener noreferrer">
                  ReactStrap
                </a>
                and developed with
                <i className="fas fa-heart"></i>
                by LaTasha Hussey
              </p>
            </Col>
          </Row>
          {/* end Development Credit */}
        </Container>
        {/* end footer Container */}
      </footer>
    </div>);
  }
}

/**
 * mapStateToProps - connects component to application state
 * @param  {object} state state of application
 * @return {object}       state.posts as this.props.comments
 */
function mapStateToProps({posts}) {
  return {posts}
}
/**
 * mapDispatchToProps - makes imported functions accessible to component via this.props
 * @return {function}
 */
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ //dispatches action that prompt state changes
    sortAllPosts: sortAllPosts
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
