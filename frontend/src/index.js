import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route} from 'react-router-dom'
import WebFont from 'webfontloader'
import 'bootstrap/dist/css/bootstrap.css';
import './styles/css/App.css';
import './styles/css/fontawesome-all.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux'
import thunk from 'redux-thunk' // lets us perfom async ops
import {logger} from 'redux-logger' // neat middleware that logs actions
import {createStore, applyMiddleware, compose} from 'redux'
import rootReducer from './reducers' // pull functions to manage state
import {loadState, saveState} from './utils/localStorage'
import createBrowserHistory from 'history/createBrowserHistory'

// Load Google Fonts
WebFont.load({
    google: {
        families: ['McLaren', 'Comfortaa', 'sans-serif']
    }
})

const history = createBrowserHistory()
const persistedState = loadState();

// Create the store with middleware to perform async ops,
// then pass combined reducers to stores
// add option to view store using Redux Dev Tools (Chrome)
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
let store = createStore(rootReducer, persistedState, composeEnhancers(applyMiddleware(thunk, logger)))

// Listen for changes to state, then save to localStorage
store.subscribe(() => {
    saveState(store.getState())
})

// Setup BrowserRouter to control views
// Pass store to Provider component
ReactDOM.render(<Provider store={store}>
    <Router history={history}>
        <Route path="/" component={App}/>
    </Router>
</Provider>, document.getElementById('root'));
registerServiceWorker();
