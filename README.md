# Readable App

This is my final project for Udacity's Redux course where I built a content and comment web app.

Users will be able to post content to predefined categories, comment on their posts and other users' posts, and vote on posts and comments.

Users will also be able to edit and delete posts and comments.

This repository includes the code for the backend API Server that I used to develop and interact with the front-end portion of the project.

## Launch Application

To launch the application:

* Install and start the API server
    - `cd api-server`
    - `npm install`
    - `node server`

* In another terminal window, start the application
    - `npm start`
